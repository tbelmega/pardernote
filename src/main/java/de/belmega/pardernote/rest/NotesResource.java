package de.belmega.pardernote.rest;

import de.belmega.pardernote.dto.NoteDTO;
import de.belmega.pardernote.service.NoteService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;


@Path("/notes")
@RequestScoped
public class NotesResource {

    public static final JsonObject EMPTY_JSON_OBJECT = Json.createObjectBuilder().build();

    @Inject
    NoteService noteService;

    @Context
    UriInfo uriInfo;

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoteDTO> getAll() {
        return noteService.getAllNotes();
    }


    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNote(@PathParam(value = "id") String id) {
        Optional<NoteDTO> note = noteService.findById(id);

        if (note.isPresent())
            return Response.ok(note.get()).build();
        else return Response.ok(EMPTY_JSON_OBJECT).build();
    }


    @Path("/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putNote(NoteDTO noteDTO, @PathParam(value = "id") String id) {
        noteService.updateNote(noteDTO);

        return Response.ok(noteDTO).build();
    }

    @Path("/")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postNote(NoteDTO noteDTO) {

        if (noteDTO.getId() != null) return Response.status(Response.Status.BAD_REQUEST)
                .entity("Note to create must not have an ID").build();

        Long id = noteService.storeNewNote(noteDTO);

        URI location = uriInfo.getAbsolutePathBuilder().path(id.toString()).build();
        System.out.println(location);
        return Response.created(location).build();
    }

}
