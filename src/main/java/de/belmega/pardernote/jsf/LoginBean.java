package de.belmega.pardernote.jsf;

import org.apache.commons.lang.StringUtils;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

@Named
@RequestScoped
public class LoginBean {

    public static final String ATTRIBUTE_USER = "username";

    private String username;
    private Object sessionUsername;

    public LoginBean() {
    }

    public String logout() {
        getHttpSession().invalidate();
        return "";
    }

    private HttpSession getHttpSession() {
        return (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }

    public Boolean getLoggedIn() {
        if (sessionUsername == null) {
            HttpSession session = getHttpSession();
            sessionUsername = session.getAttribute(ATTRIBUTE_USER);
        }

        boolean sessionHasLoggedInUser = sessionUsername != null;
        return sessionHasLoggedInUser;
    }

    public void login() {
        if (StringUtils.isEmpty(username)) return;

        HttpSession session = getHttpSession();
        session.setAttribute(ATTRIBUTE_USER, username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGreeting() {
        String username = (String) getHttpSession().getAttribute(ATTRIBUTE_USER);
        return "Hallo, " + username + ".";
    }

    public String getGreetingGuest() {
        return "Hallo, Gast.";
    }
}
