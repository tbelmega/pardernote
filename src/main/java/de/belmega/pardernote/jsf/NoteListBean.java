package de.belmega.pardernote.jsf;

import de.belmega.pardernote.dto.NoteDTO;
import de.belmega.pardernote.service.NoteService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class NoteListBean {

    @Inject
    NoteService noteService;
    private int orderByTitleAscending;

    private List<NoteDTO> allNotes;

    public List<NoteDTO> getNotes() {
        if (allNotes == null) {
            allNotes = noteService.getAllNotes();
        }
        return allNotes;
    }
}
