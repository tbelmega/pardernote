package de.belmega.pardernote.jsf;

import de.belmega.pardernote.dto.NoteDTO;
import de.belmega.pardernote.service.NoteService;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

@Named
@ConversationScoped
public class NoteDetailsBean implements Serializable {

    private String noteId;

    private NoteDTO note;

    @Inject
    NoteService noteService;

    @Inject
    private Conversation conversation;

    public void loadNote() {
        if (this.note != null) return;

        conversation.begin();
        if (noteId == null) throw new IllegalArgumentException("Cannot load Note without ID.");

        Optional<NoteDTO> note = noteService.findById(noteId);
        if (!note.isPresent()) throw new IllegalArgumentException("Note with this ID does not exist.");

        this.note = note.get();
        System.out.println("Loaded note:" + this.note);
    }

    public String save() {
        System.out.println(this.note);
        noteService.updateNote(this.note);
        return back();
    }

    public String back() {
        conversation.end();
        return "notelist.xhtml?faces-redirect=true";
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public NoteDTO getNote() {
        return note;
    }

    public void setNote(NoteDTO note) {
        this.note = note;
    }

    public String archive() {
        note.setArchive(true);
        return save();
    }
}
