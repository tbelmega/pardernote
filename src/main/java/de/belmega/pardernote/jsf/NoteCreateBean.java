package de.belmega.pardernote.jsf;

import de.belmega.pardernote.dto.NoteDTO;
import de.belmega.pardernote.service.NoteService;
import org.apache.commons.lang.StringUtils;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import static de.belmega.pardernote.jsf.LoginBean.ATTRIBUTE_USER;

@Named
@RequestScoped
public class NoteCreateBean {

    public static final String NOTELIST_PAGE = "notelist?faces-redirect=true";
    public static final String INDEX_PAGE = "index?faces-redirect=true";

    @Inject
    NoteService noteService;

    private NoteDTO noteDTO;

    @PostConstruct
    public void init() {
        this.noteDTO = new NoteDTO();
    }

    public String clear() {
        this.noteDTO = new NoteDTO();
        return "";
    }

    public String save() {
        saveNewNote();
        return NOTELIST_PAGE;
    }

    private void saveNewNote() {
        String author = (String) getHttpSession().getAttribute(ATTRIBUTE_USER);
        if (StringUtils.isEmpty(author))
            author = "Gast";

        this.noteDTO.setAuthor(author);
        this.noteDTO.setCreationTimestamp(System.currentTimeMillis());
        noteService.storeNewNote(this.noteDTO);
    }


    private HttpSession getHttpSession() {
        return (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }

    public String saveAndNew() {
        saveNewNote();
        return INDEX_PAGE;
    }

    public NoteDTO getNote() {
        return noteDTO;
    }

    public void setNote(NoteDTO note) {
        this.noteDTO = note;
    }
}
