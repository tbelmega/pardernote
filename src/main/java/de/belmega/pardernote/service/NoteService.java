package de.belmega.pardernote.service;

import de.belmega.pardernote.dto.NoteDTO;
import de.belmega.pardernote.persistence.dao.NoteDAO;
import de.belmega.pardernote.persistence.entities.NoteEntity;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Stateless
@Dependent
public class NoteService implements Serializable {

    private static final Logger LOG = Logger.getLogger(NoteService.class);

    @Inject
    NoteDAO noteDAO;

    public long storeNewNote(NoteDTO note) {
        NoteEntity noteEntity = createNoteEntity(note);
        NoteEntity persisted = noteDAO.persist(noteEntity);
        return persisted.getId();
    }

    public List<NoteDTO> getAllNotes() {
        List<NoteEntity> allNotes = noteDAO.findAllNotArchived();

        LOG.warn("Number of notes: " + allNotes.size());
        List<NoteDTO> resultDTOs = new LinkedList<>();
        for (NoteEntity note : allNotes)
            resultDTOs.add(createNoteDTO(note));

        return resultDTOs;
    }


    public Optional<NoteDTO> findById(String noteId) {
        long id = Long.parseLong(noteId);
        Optional<NoteEntity> entity = noteDAO.findById(id);
        if (entity.isPresent())
            return Optional.of(createNoteDTO(entity.get()));
        else return Optional.empty();
    }

    public void updateNote(NoteDTO noteDTO) {
        if (noteDTO.getId() == null)
            throw new IllegalArgumentException("Cannot update a note without ID.");

        Optional<NoteEntity> note = noteDAO.findById(noteDTO.getId());

        if (!note.isPresent())
            throw new IllegalArgumentException("Cannot update a note that does not exist.");

        NoteEntity noteEntity = note.get();
        fillNoteEntityFromDTO(noteDTO, noteEntity);
        noteDAO.persist(noteEntity);
    }

    private NoteEntity createNoteEntity(NoteDTO noteDTO) {
        NoteEntity noteEntity = new NoteEntity();
        fillNoteEntityFromDTO(noteDTO, noteEntity);
        return noteEntity;
    }

    private void fillNoteEntityFromDTO(NoteDTO noteDTO, NoteEntity noteEntity) {
        noteEntity.setAuthor(noteDTO.getAuthor());
        noteEntity.setArchive(noteDTO.isArchive());
        noteEntity.setCreationTimestamp(noteDTO.getCreationTimestamp());
        noteEntity.setDone(noteDTO.isDone());
        noteEntity.setPriority(noteDTO.getPriority());
        noteEntity.setText(noteDTO.getText());
        noteEntity.setTitle(noteDTO.getTitle());
        Date todoUntil = noteDTO.getTodoUntil();
        if (todoUntil != null)
            noteEntity.setTodoUntil(todoUntil.getTime());
    }

    private NoteDTO createNoteDTO(NoteEntity noteEntity) {
        NoteDTO noteDTO = new NoteDTO(noteEntity.getId());

        noteDTO.setAuthor(noteEntity.getAuthor());
        noteDTO.setArchive(noteEntity.isArchive());
        noteDTO.setCreationTimestamp(noteEntity.getCreationTimestamp());
        noteDTO.setDone(noteEntity.isDone());
        noteDTO.setPriority(noteEntity.getPriority());
        noteDTO.setText(noteEntity.getText());
        noteDTO.setTitle(noteEntity.getTitle());

        Long todoUntil = noteEntity.getTodoUntil();
        if (todoUntil != null)
            noteDTO.setTodoUntil(new Date(todoUntil));

        return noteDTO;
    }
}
