package de.belmega.pardernote.service;

import de.belmega.pardernote.dto.NoteDTO;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

@ApplicationScoped
public class TestDataGenerator {

    @PersistenceContext
    EntityManager em;

    private static final Logger LOG = Logger.getLogger(TestDataGenerator.class);

    @Inject
    NoteService noteService;


    public void setupTestData(@Observes @Initialized(ApplicationScoped.class) Object init) {
        NoteDTO note1 = new NoteDTO();
        note1.setText("Visit 9gag.com");
        note1.setCreationTimestamp(System.currentTimeMillis() - 4000000);
        note1.setAuthor("Gast");
        note1.setPriority(10);
        note1.setTodoUntil(new Date());
        note1.setDone(true);
        noteService.storeNewNote(note1);

        NoteDTO note2 = new NoteDTO();
        note2.setText("Chill for a while");
        note2.setCreationTimestamp(System.currentTimeMillis() - 3000000);
        note2.setAuthor("Gast");
        noteService.storeNewNote(note2);

        NoteDTO note3 = new NoteDTO();
        note3.setText("Act important");
        note3.setCreationTimestamp(System.currentTimeMillis() - 2000000);
        note3.setAuthor("Gast");
        noteService.storeNewNote(note3);

        NoteDTO note4 = new NoteDTO();
        note4.setText("Profit.");
        note4.setCreationTimestamp(System.currentTimeMillis() - 1000000);
        note4.setAuthor("Gast");
        noteService.storeNewNote(note4);

        LOG.warn("Test data injection complete.");
    }


}
