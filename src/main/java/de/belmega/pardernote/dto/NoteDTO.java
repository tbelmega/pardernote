package de.belmega.pardernote.dto;


import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.Optional;

public class NoteDTO {
    private final Long id;

    private String text;
    private Long creationTimestamp;
    private String title;
    private String author;
    private boolean done;
    private boolean archive;
    private Date todoUntil;
    private long priority;

    public NoteDTO(long id) {
        this.id = id;
    }

    public NoteDTO() {
        this.id = null;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        if (StringUtils.isEmpty(this.title) && !StringUtils.isEmpty(this.text)) {
            // Show at most 30 characters of the note text
            if (this.text.length() <= 30) return this.text;
            else return this.text.substring(0, 30) + "...";
        } else return this.title;
    }

    public String getAuthor() {
        return author;
    }

    public Long getId() {
        return id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public Date getTodoUntil() {
        return todoUntil;
    }

    public void setTodoUntil(Date todoUntil) {
        this.todoUntil = todoUntil;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }
}
