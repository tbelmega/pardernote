package de.belmega.pardernote.persistence.dao;

import de.belmega.pardernote.persistence.entities.NoteEntity;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Stateless
@Dependent
public class NoteDAO {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public NoteEntity persist(NoteEntity noteEntity) {
        NoteEntity newNoteEntity = em.merge(noteEntity);
        return newNoteEntity;
    }

    public List<NoteEntity> findAllNotArchived() {
        CriteriaBuilder criteria = em.getCriteriaBuilder();
        CriteriaQuery<NoteEntity> query = criteria.createQuery(NoteEntity.class);
        Root<NoteEntity> entityRoot = query.from(NoteEntity.class);

        query.where(criteria.isFalse(entityRoot.get("archive")));

        query.select(entityRoot);
        List<NoteEntity> resultList = em.createQuery(query).getResultList();

        return resultList;
    }

    public Optional<NoteEntity> findById(@NotNull long id) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<NoteEntity> criteria = builder.createQuery(NoteEntity.class);
        Root<NoteEntity> from = criteria.from(NoteEntity.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));

        TypedQuery<NoteEntity> typed = em.createQuery(criteria);
        Optional<NoteEntity> noteEntity = typed.getResultList().stream().findFirst();
        return noteEntity;
    }

}
